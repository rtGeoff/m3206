#!/bin/bash
p="first"
d="last"
if [ $1 != $p ] && [ $1 != $d ]
then
echo "First argument should be first or last."
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi
if [ ! -e $3 ]#
then
echo "Le fichier "$3" n'existe pas."
echo "exemple: ./datecomm.sh [first|last] commande fichier"
exit 1
fi
if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]
then
echo "La commande prends 3 paramètres."
echo "exemple: ./datecomm.sh [first|last] commande fichier"
exit 1
fi
nbOccurence=$(cat my_history | awk '{print $2}' | grep $2 | cut -d ';' -f2 | wc -l) 


if [ $nbOccurence -eq 0 ]  
then
echo "history ne contien pas la commande "$2
exit 1
fi
if [ $1 = $p ]
then
tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)
fi
if [ $1 = $d ] 
then
tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)

fi
date -d @$tstamp
