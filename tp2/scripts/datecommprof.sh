#!/bin/bash
tstamp=$(cat $2 | awk '{print $2}' | grep "$1" | cut -d ':' -f1 | head -1)
date -d @$tstamp
