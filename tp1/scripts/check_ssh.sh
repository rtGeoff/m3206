#! /bin/bash

test=`dpkg -l | grep ssh`

if [ -z "$test" ]
then 
	echo "Le programme $tools n'est pas installé"
else
	test2=`pgrep ssh`
	if [ -z "$test" ]
	then
		echo "Le programme n'est pas lancé, nous le lançon pour vous."
		service ssh start
	else
		echo "Le programme est bien lancé."
	fi
fi
