#! /bin/bash

if [ $(whoami)='root' ]; then
	echo '[+++ Mise à jour en cours +++]'
	apt-get update
	apt-get upgrade
else

  echo "[+++ Merci de lancer ce script avec les droits root +++]"

fi
