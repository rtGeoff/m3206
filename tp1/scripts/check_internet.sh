#!/bin/bash

WGET="usr/bin/wget"

echo "[...] Checking internet connection [...]"

$WGET -q --tries=20 --timeout=10 http://google.com -O /tmp/goodle.idx &> /dev/null
if [ ! -s /tmp/google.idx ]
then
	echo "[/!\] Not connected to Internet    [/!\]"
	echo "[/!\] Please check configuration   [/!\]"
else
	echo "[...] Internet access OK           [...]"
fi
