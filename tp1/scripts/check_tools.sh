#! /bin/bash

for tools in git minicom tmux vim htop
do
test=`dpkg -l | grep $tools`

if [ -z "$test" ]
then 
echo "Le programme $tools n'est pas installé"
else
	echo "Le programme $tools est bien installé"
fi
done
