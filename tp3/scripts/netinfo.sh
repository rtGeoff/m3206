#!/bin/bash
adIP=$(ifconfig eth0 2>/dev/null | grep "inet addr" | awk '{print $2}' | awk 'BEGIN {FS = ":" } ; {print $2}')

servDNS=$(cat /etc/resolv.conf | awk '{print $2}' | head -1)

routeDef=$(route | grep default | awk '{print $2}')

hname=$(hostname)

upti=$(uptime | awk '{print $3}' | awk 'BEGIN {FS = ","} ; {print $1}')

port21=$(nmap 127.0.0.1 | grep "22/tcp" | awk '{print $3}')

port80=$(nmap 127.0.0.1 | grep "80/tcp" | awk '{print $3}')

echo "hostname:			"$hname
echo "uptime:			"$upti
echo "adresse IP:		"$adIP
echo "route par défaut		"$routeDef
echo "serveur DNS		"$servDNS
echo "service sur le port 22:	"$port21
echo "service sur le port 80:	"$port80
