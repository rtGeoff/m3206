#!/bin/bash
mkdir -p Music
mkdir -p Documents
mkdir -p Archives
mkdir -p Pictures
mkdir -p Inclassables
mkdir -p Videos
for fichier in $(ls) #ls renvoie tous les fichiers de l'arborescence courante
do
typeFichier=$(file --mime-type -b $fichier  | awk 'BEGIN{FS="/"} {print $1}')
sousTypeFichier=$(file --mime-type -b $fichier  | awk 'BEGIN{FS="/"} {print $2}')
echo $typeFichier
case "$typeFichier" in #pour chaque type de fichier, on va le ranger dans le bon répertoire
"image" ) #si le fichier est une image on le met dans Pictures
echo "fichier image";mv $fichier Pictures/. ;;
"audio" ) #si le fichier est une fichier audio, on le met dans Music
echo "fichier audio";mv $fichier Music/. ;;
"application") #si le fichier est un fichier application, plusieurs cas sont possibles
echo "fichier application";
case "$sousTypeFichier" in
"pdf" )#si le fichier est un fichier pdf, on le mets dans le répertoire Documents 
echo "fichier pdf";mv $fichier Documents/. ;;
vnd.* )
echo "fichier Word docx";mv $fichier Documents/. ;;
"zip")#si le fichier est une archive
echo "fichier archivé"; mv $fichier Archives/. ;;

esac ;;
*)
echo "fichier inclassable" ;;
 
esac
done
